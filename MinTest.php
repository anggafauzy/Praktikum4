<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	
	class MinTest extends TestCase{
		/**
		*@dataProvider additionProvider
		*/
		public function testAdd($a, $b, $expected)
			{
				$this->assertSame($expected, $a - $b);
				
			}
		public function additionProvider()
		{
			return [
			[0, 0, 0],
			[2, 1, 1],
			[6, 5, 1],
			[13, 7, 6],
			];
		}
	}
?>