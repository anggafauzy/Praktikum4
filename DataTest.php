<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	
	class DataTest extends TestCase{
		/**
		*@dataProvider additionProvider
		*/
		public function testAdd($a, $c, $expected)
			{
				$this->assertSame($expected, $a + $c);
				
			}
		public function additionProvider()
		{
			return [
			[0, 0, 0],
			[0, 1, 1],
			[1, 0, 1],
			[1, 1, 2],
			];
		}
	}
?>